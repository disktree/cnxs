
## CNXS

CP = -cp src \
	-cp ../../lib/pixi.hx/src/extern/ \
	-cp ../../lib/tween.hx/src/extern \
	-cp ../../lib/om/src
HX = haxe $(CP) -main cnxs.App -dce full -debug
LESSC = lessc --no-color

all: build

build/cnxs.css: res/cnxs.less
	@mkdir -p build
	$(LESSC) res/cnxs.less build/cnxs.css

build/cnxs.js: res/* src/cnxs/*.hx
	@mkdir -p build
	$(HX) -js $@

client: build/cnxs.js build/cnxs.css

server: src/cnxs/HighscoreServer.hx
	haxe -main cnxs.HighscoreServer -php server -cp src

build: client server
	@mkdir -p build
	@rsync res/index.html build
	@rsync res/robots.txt build
	@rsync res/sitemap.xml build
	@rsync -r res/fonts build
	@rsync -r res/images build
	@rsync -r res/music build
	@rsync -r res/js build
	@rsync -r res/sounds build

clean:
	rm -rf build
	
.PHONY: all build clean
