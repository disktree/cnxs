package cnxs;

//TODO onError handling callback

import haxe.Json;

typedef HighscoreData = {
	var user : String;
	var date : String;
	var score : Int;
}

class Highscores {

	//TODO
	//static inline var URL = "http://192.168.0.14/games/cnxs/server/index.php";
	static inline var URL = "http://localhost/games/cnxs/server/index.php";
	//static inline var URL = "http://games.disktree.net/cnxs2/server/index.php";

	public static function submit( user : String, score : Int, callback : String->Void ) {
		var data = Json.stringify({
			id : "submit",
			user : user,
			score : score,
		});
		request( data, function(result){
			trace(result);
		});
	}

	//public static function hasUsername( username : String ) {}

	public static function get( callback : Array<HighscoreData>->Void ) {
		var data = Json.stringify({
			id : "get"
		});
		request( data, function(result){
			var json = Json.parse( result );
			callback( json );
		});
	}

	static function request( data : String, callback : String->Void ) {
		var r = new haxe.Http( URL );
		r.setPostData( data );
		r.onData = callback;
		r.onError = callback;
		r.request( true );
	}
}
