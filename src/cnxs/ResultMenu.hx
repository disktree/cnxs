package cnxs;

import js.Browser.document;
import js.html.DivElement;
import js.html.InputElement;
import js.html.ButtonElement;

class ResultMenu {

	public var e(default,null) : DivElement;

	var e_username : InputElement;
	var e_submit : ButtonElement;

	public function new() {
		e = document.createDivElement();
		e.id = 'resultmenu';
		e.style.display = 'none';
	}

	public function show( points : Int ) {
		
		var e_points = document.createDivElement();
		e_points.innerText = 'YOU SCORED ' + points + ' POINTS';
		e.appendChild( e_points );

		e_username = document.createInputElement();
		e_username.oninput = function(e){
			//var username = e_username.value;
			if( e_username.value.length == 0 ) {
				e_submit.setAttribute( 'disabled', null );
			} else {
				e_submit.removeAttribute( 'disabled' );
			}
		}
		e.appendChild( e_username );

		e_submit = document.createButtonElement();
		e_submit.innerText = 'SUBMIT';
		e_submit.setAttribute( 'disabled', null );
		e_submit.onclick = function(_) submitScore();
		e.appendChild( e_submit );

		//var storedUsernames = Storage.getStoredUsernames();
		//trace( storedUsernames );

		//var e = document.createElement();
		//e.innerHTML = '<br>';
		//e.appendChild(e);

		var e_playagain = document.createDivElement();
		e_playagain.classList.add('button');
		e_playagain.innerText = 'PLAY AGAIN';
		e_playagain.onclick = function(_){
			State.set( StatePlay );
		}
		e.appendChild( e_playagain );
		

		var scoreBoard = new HighscoreBoard();
		e.appendChild( scoreBoard.e );


		//TODO
		/*
		var e_twitter = document.createDivElement();
		e_twitter.innerHTML = '<a href="https://twitter.com/share" class="twitter-share-button" data-url="http://games.disktree.net/cnxs" data-text="I just scored $points points at CNXS" data-count="none">Tweet</a><script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?"http":"https";if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document, "script", "twitter-wjs");</script>';
		e.appendChild( e_twitter );
		*/

		//visible = !visible;

		//e.style.display = visible ? 'block' : 'none';
		e.style.display = 'block';
	}

	function submitScore() {

		var username = e_username.value;
		trace( username );

		Storage.storeUsername( username );
	}
}
