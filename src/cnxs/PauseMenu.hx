package cnxs;

import js.Browser.document;
import js.html.DivElement;

class PauseMenu {

	public var e(default,null) : DivElement;

	//var visible = false;

	public function new() {

		e = document.createDivElement();
		e.id = 'pausemenu';
		e.innerText = 'PAUSE';
		e.style.display = 'none';
	}

	public function show() {
		e.style.display = 'block';
	}

	public function hide() {
		e.style.display = 'none';
	}
}