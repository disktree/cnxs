package cnxs;

typedef LevelColorScheme = {
	var background : Int;
	var pipeRegular : Int;
	var pipeActive : Int;
	var grid : Int;
}

typedef LevelData = {
	var pipeTypeRatio : Float;
	var colors : LevelColorScheme;
	var time : Int;
	var numPipesToSolve : Int;
	//var specials
}

class Level {

	static var list : Array<LevelData> = [
		{
			pipeTypeRatio : 0.8,
			colors : {
				background: 0x2E4259,
				pipeRegular: 0x03C8FA,
				pipeActive: 0xF7483B,
				grid: 0x737373
			},
			time : 60,
			numPipesToSolve : 50,
		},
		{
			pipeTypeRatio : 0.75,
			colors : {
				background: 0x000137,
				pipeRegular: 0x79003D,
				pipeActive: 0xF89801,
				grid: 0x4C0075
			},
			time : 60,
			numPipesToSolve : 50,
		},
		{
			pipeTypeRatio : 0.65,
			colors : {
				background: 0xF32E23,
				pipeRegular: 0xFF9E26,
				pipeActive: 0xB0F100,
				grid: 0xFF0067
			},
			time : 60,
			numPipesToSolve : 50,
		},
		{
			pipeTypeRatio : 0.55,
			colors : {
				background: 0x1E1E20,
				pipeRegular: 0xDC3522,
				pipeActive: 0xD9CB9E,
				grid : 0x374140
			},
			time : 60,
			numPipesToSolve : 50
		},
		{
			pipeTypeRatio : 0.55,
			colors : {
				background: 0x000000,
				pipeRegular: 0x88CFA9,
				pipeActive: 0xFFFFCC,
				grid : 0x363636
			},
			time : 60,
			numPipesToSolve : 50
		},
	];
	
	public static inline function get( n : Int ) : LevelData  {
		return list[n];
	}

}
