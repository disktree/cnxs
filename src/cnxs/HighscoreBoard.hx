package cnxs;

import js.Browser.document;
import js.html.DivElement;
import js.html.SpanElement;

class HighscoreBoard {

	static inline var MAX_ENTRIES_TO_SHOW = 10;

	public var e(default,null) : DivElement;
	
	public function new() {

		e = document.createDivElement();
		e.id = 'scoreboard';

		load();
	}

	function load() {
		Highscores.get( function(data){
			for( entry in data ) {
				
				var user = entry.user;
				var dateTime = xTimeAgo( Date.fromString( entry.date ) );
				var score = entry.score;

				var e = document.createDivElement();
				var html = '<div class="entry"><span class="user">$user</span>/<span class="score">$score</span>/<span class="date">$dateTime</span></div>';
				e.innerHTML = html;
				this.e.appendChild(e);
			}
		});
	}

	static function xTimeAgo( date : Date ) : String {
		var diff = ( Date.now().getTime() - date.getTime() ) / 1000;
		var day_diff = Math.floor( diff / 86400 );
		if( Math.isNaN( day_diff ) || day_diff < 0 || day_diff >= 31 )
			return null;
		if( day_diff == 0 ) {
			if( diff < 60 ) return "just now";
			else if( diff < 120 ) return "1 minute ago";
			else if( diff < 3600 ) return Math.floor( diff/60 ) + " minutes ago";
			else if( diff < 7200 ) return "1 hour ago";
			else if( diff < 86400 ) return Math.floor( diff/3600 )+" hours ago";
		} else {
			if( day_diff == 1 ) return "Yesterday";
			else if( day_diff < 7 ) return day_diff+" days ago";
			else if( day_diff < 31 ) return Math.ceil( day_diff/7)+" weeks ago";
		}
		return null;
	}

}