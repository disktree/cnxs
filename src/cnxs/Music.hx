package cnxs;

import js.html.Audio;

class Music {

	public static var volume(get,set) : Float;

	//static var map : StringMap<Audio>;
	static var audio1 : Audio;
	static var audio2 : Audio;
	static var _volume : Float = 0.5;

	static inline function get_volume() : Float return _volume;
	static function set_volume( v : Float ) : Float {
		if( audio1 != null ) audio1.volume = v;
		if( audio2 != null ) audio2.volume = v;
		return _volume = v;
	}

	/*
	public static function setup( files : Array<String>, onLoad : Void->Void ) {
		var numLoaded = 0;
		map = new StringMap();
		for( file in files ) {
			var a = new Audio();
			a.addEventListener( 'canplaythrough', function(e) {
				if( ++numLoaded == files.length )
					onLoad();
			});
			a.src = 'sounds/$file';
			map.set( file, a );
		}
	}
	*/

	public static function play( id : String, ?volume : Float, loop = true ) {
		if( volume != null ) _volume = volume;
		audio1 = new Audio();
		audio1.src = 'music/$id';
		audio1.loop = loop;
		audio1.volume = _volume;
		audio1.play();
	}

	/*
	public static function mixTo( id : String, ?volume : Float, loop = true ) {
		if( audio1 == null ) {
			//play( id, volume, );
		}
	}
	*/

}
