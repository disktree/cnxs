package cnxs;

import js.Browser.document;
import js.html.DivElement;
import js.html.ImageElement;

class GameMenu {

	public var e(default,null) : DivElement;
	public var time(null,set) : Int;
	public var score(default,set) : Int;
	public var level(null,set) : Int;

	var e_level : DivElement;
	var e_time : DivElement;
	var e_score : DivElement;

	public function new() {

		//clockwise = true;

		e = document.createDivElement();
		e.id = 'gamemenu';

		e_level = document.createDivElement();
		e_level.innerText = 'LEVEL 1';
		e.appendChild( e_level );

		e_time = document.createDivElement();
		e_time.innerText = '00:00';
		e.appendChild( e_time );

		e_score = document.createDivElement();
		e_score.innerText = '0';
		e.appendChild( e_score );

		//TODO time-bar
		//TODO best shape
	}

	function set_time( v : Int ) : Int {
		var p = DateTools.parse(v);
		var s = "";
		if( p.minutes < 10 ) s += "0";
		s += p.minutes;
		s += ":";
		if( p.seconds < 10 ) s += "0";
		s += p.seconds;
		e_time.innerText = s;
		return v;
	}

	function set_level( v : Int ) : Int {
		e_level.innerText = 'LEVEL '+v;
		return v;
	}

	function set_score( v : Int ) : Int {
		var s = Std.string(v);
		e_score.innerText = s;
		return v;
	}

	public function setColor( color : Int ) {
		e_level.style.color = e_time.style.color = '#' + untyped color.toString(16);
	}

}
