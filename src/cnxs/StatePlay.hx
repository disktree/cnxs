package cnxs;

import js.Browser.document;
import js.Browser.window;
import haxe.Timer;
import cnxs.Level;

enum PlayState {
	start;
	play;
	pause;
	nextlevel;
	gameover;
}

@:keep
class StatePlay extends State {

	static inline var MENU_UPDATE_INTERVAL = 250;

	//public static var level(default,null) : LevelData;

	public static var renderer(default,null) : Dynamic; //pixi.Renderer; //TODO

	var stage : pixi.Stage;
	var board : Board;
	var menu : GameMenu;
	var pauseMenu : PauseMenu;
	var resultMenu : ResultMenu;
	
	var state : PlayState;
	var levelStartTime : Int;
	var numPipesSolved = 0;
	var numPipesSolvedTotal = 0;
	var points = 0;
	var timeAvailable : Int;
	var timeRemaining : Int;
	var pauseStartTime : Int;
	var timer : Timer;

	var levelIndex = 0;
	var level : LevelData;

	public override function init() {

		super.init();

		state = start;
		
		var boardDimension = 10;
		var boardSize = 400;

		if( App.MOBILE )
			boardSize = window.innerWidth-1; //TODO wtf -1
		
		var canvas = document.createCanvasElement();
		canvas.width = canvas.height = boardSize;
		e.appendChild( canvas );
		stage = new pixi.Stage( 0x000000, true );
		renderer = Pixi.autoDetectRenderer( boardSize, boardSize, canvas, true, false );

		board = new Board( canvas, boardDimension, boardSize );
		board.onSolve = onBoardSolve;
		stage.addChild( board );

		menu = new GameMenu();
		e.appendChild( menu.e );

		pauseMenu = new PauseMenu();
		e.appendChild( pauseMenu.e );

		resultMenu = new ResultMenu();
		e.appendChild( resultMenu.e );
		//resultMenu.show( 39586 );

		timer = new Timer( MENU_UPDATE_INTERVAL );
		timer.run = updateMenu;
		
		window.requestAnimationFrame( untyped update );
	
		window.addEventListener( 'keydown', onKeyDown, false );
		window.addEventListener( 'keyup', onKeyUp, false );

		nextLevel();

		//Music.mixTo( 'Birdscape C.ogg', 0.2 );
	}

	public override function close() {
		super.close();
		window.removeEventListener( 'keydown', onKeyDown );
	}

	function nextLevel() {

		trace("loading level "+levelIndex );

		this.level = Level.get( levelIndex ) ;

		menu.setColor( level.colors.pipeRegular );
		menu.level = levelIndex + 1;

		timeAvailable = level.time * 1000;
		levelStartTime = now();

		board.startLevel( level );

		state = play;

		/*
		window.addEventListener( 'keydown', onKeyDown, false );
		window.addEventListener( 'keyup', onKeyUp, false );

		timer.run = updateMenu;
		window.requestAnimationFrame( untyped update );
		*/
	}

	function update( time : Float ) {

		TWEEN.update( time );
		board.update( time );

		switch state {
		case start:
		case play:

			var timeElapsed = now() - levelStartTime;
			timeRemaining =  timeAvailable - timeElapsed;

			if( timeRemaining <= 0 ) { // Game over
				board.pause();
				resultMenu.show( points );
				state = gameover;
			}

			if( numPipesSolved >= level.numPipesToSolve ) {
				trace( "level completed" );
				numPipesSolved = 0;
				state = nextlevel;
				board.endLevel( function(){
					levelIndex++;
					nextLevel();
				});
			}

		case pause:
		case nextlevel:
		case gameover:
		}

		renderer.render( stage );
		window.requestAnimationFrame( untyped update );

		/*
		TWEEN.update( time );
		board.update( time );
		
		var timeElapsed = now() - levelStartTime;
		timeRemaining =  timeAvailable - timeElapsed;

		if( timeRemaining <= 0 ) { // Game over
			board.pause();
			resultMenu.show( points );
			return;
		}

		if( points >= level.pointLimit ) {
			trace("NEXTLEVEL!");
			board.clear( function(){

			});
			//levelIndex++;
			//loadNextLevel();
		}

		renderer.render( stage );
		window.requestAnimationFrame( untyped update );
		*/
	}

	function updateMenu() {
		if( state == play ) {
			menu.time = timeRemaining;
		}
	}

	function onBoardSolve( pipes : Array<Pipe> ) {
		
		numPipesSolved += pipes.length;
		numPipesSolvedTotal += pipes.length;

		//TODO calculate points
		//TODO add more time

		var timeToAdd = pipes.length; //TODO calculate
		timeAvailable += timeToAdd * 1000;

		points += pipes.length;
		menu.score = points;
	}

	function onKeyDown(e) {
		//trace(e.keyCode);
		switch e.keyCode {
		case 17: //ctrl
			Board.rotateClockwise = false;
		case 27: //esc
			switch state {
			case play:
				pauseStartTime = now();
				state = pause;
				board.pause();
				pauseMenu.show();
			case pause:
				timer.stop();
				levelStartTime += now() - pauseStartTime;
				state = play;
				board.resume();
				pauseMenu.hide();
				timer = new Timer( MENU_UPDATE_INTERVAL );
				timer.run = updateMenu;
			default:
			}
		}
	}

	function onKeyUp(e) {
		switch e.keyCode {
		case 17: //ctrl
			Board.rotateClockwise = true;
		}
	}

	static inline function now() : Int return Std.int( Date.now().getTime() );
}
