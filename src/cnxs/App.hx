package cnxs;

import js.Browser.document;
import js.Browser.window;

class App {

	public static var WIDTH = 400;
	public static var HEIGHT = 580;

	public static var MOBILE = false;

	static function onWindowResize(e) {
		State.current.resize( window.innerWidth, window.innerHeight );
	}

	static function main() {

		Pixi.dontSayHello = true;

		window.onload = function(_){

			#if debug
			trace( "CNXS "+Cnxs.VERSION + #if debug "(debug)" #end );
			#end
			
			if( untyped __js__('/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)') ) {
				MOBILE = true;
				WIDTH = window.innerWidth;
				HEIGHT = window.innerHeight;
			}

			/*
			Highscores.submit( 'tong', Std.int(Math.random()*1000), function(result){
				trace(result);
				if( result == 'ok' ) {
					//HighscoreServer.get();
				}
			});
			return;
			*/

			//document.body.innerHTML = '<div class="g-plusone" data-size="medium" data-annotation="none"></div>';
			//return;

			document.getElementById('header').style.display = 'none';

			//Sound.setup( ['Birdscape C.ogg'], function(){
			/*
			Music.setup( ['Birdscape C.ogg'], function(){

				//trace("LOOADED");

				State.setup( document.getElementById('game') );
				State.set( StateMainMenu );
				//State.set( StatePlay );

				window.addEventListener( 'resize', onWindowResize, false );
			});
			*/

			State.setup( document.getElementById('game') );
			State.set( StateMainMenu );
			
			window.addEventListener( 'resize', onWindowResize, false );
		}
	}
}
