package cnxs;

import js.html.Audio;
import haxe.ds.StringMap;

class Sound {

	public static var volume = 0.5;

	static var map : StringMap<Audio>;

	public static function setup() {
		map = new Map();
		for( file in ["Game FX 09"] ) { //TODO
			var a = new Audio();
			a.src = 'sounds/$file.mp3';
			map.set( file, a );
		}
	}

	public static function play( id : String, ?volume : Float ) {

		var _volume = (volume == null) ? Sound.volume : volume;
		
		var a = map.get( id );
		a.volume = _volume;
		a.play();
	}
}