package cnxs;

import js.Browser.document;
import js.html.Element;

@:keep
class State {

	var id : String;
	var e : Element;

	function new( id : String ) {
		if( id == null )
			id = Type.getClassName( Type.getClass( this ) ).substr(10).toLowerCase();
		this.id = id;
	}

	public function init() {
		e = document.createDivElement();
		e.classList.add('state');
		e.id = id;
	}

	public function close() {
	}

	public function resize( width : Int, height : Int ) {
	}

	public static var current(default,null) : State;

	static var container : Element;

	public static function set( s : Class<State>, ?args : Array<Dynamic> ) {
		if( args == null ) args = [];
		if( current != null ) {
			current.close();
			current.e.remove();
		}
		var n = Type.createInstance( s, args );
		current = n;
		n.init();
		container.appendChild( n.e );
	}

	public static function setup( container : Element ) {
		//container.innerHTML = '';
		State.container = container;
	}

}
