package cnxs;

import haxe.Json;

using Lambda;

class Storage {

	static inline var PREFIX = "cnxs_";

	static var storage = js.Browser.getLocalStorage();

	public static inline function clearAll() {
		storage.clear();
	}

	public static function getStoredUsernames() : Array<String> {
		var d = getItem( "usernames" );
		if( d == null )
			return [];
		return d;
	}

	public static function storeUsername( name : String ) {
		var alreadyStored = getStoredUsernames();
		if( !alreadyStored.has( name ) ) {
			alreadyStored.unshift( name );
			setItem( 'usernames', alreadyStored );
		}
	}

	static function getItem( id : String ) {
		var d = storage.getItem( PREFIX + id );
		if( d == null )
			return null;
		return Json.parse( d );
	}

	static function setItem( id : String, data : Dynamic ) {
		storage.setItem( PREFIX + id, Json.stringify( data ) );
	}

}
