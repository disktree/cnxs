package cnxs;

#if sys

import Sys.print;
import sys.db.Types;
import haxe.Json;
import php.Web;

using Lambda;

@:table("entries")
@:id(uid)
class Entry extends sys.db.Object {
	public var uid : SInt;
	public var user : SString<128>;
	public var date : SDate;
	public var score : SInt;
	public var ip : SString<128>;
	public var uri : SString<256>;
}

/**
	Highscore data server
*/
class HighscoreServer {

	static inline var DB_FILE = "cnxs.db";

	static function main() {
		
		try {

			if( !sys.FileSystem.exists( DB_FILE ) ) {
				sys.io.File.write( DB_FILE );
			}

			var cnx = sys.db.Sqlite.open( DB_FILE );
			sys.db.Manager.cnx = cnx;
			sys.db.Manager.initialize();
			if( !sys.db.TableCreate.exists( Entry.manager ) ) sys.db.TableCreate.create( Entry.manager );
			
			//print( Web.getPostData() );
			//return;

			var data = Json.parse( Web.getPostData() );

			switch data.id {

			case 'get':
				var entries = Entry.manager.all();
				var arr = new Array<Dynamic>();
				for( e in entries ) {
					arr.push({
						user : e.user,
						date : e.date,
						score : e.score
					});
				}
				print( Json.stringify( arr ) );
				return;

			case "submit":
				var e = new Entry();
				e.user = data.user;
				e.score = Std.parseInt( data.score );
				e.date = Date.now();
				e.ip = Web.getClientIP();
				e.uri = Web.getURI();
				e.insert();

			}

			/*
			var params = Web.getParams();
			
			switch params.get('id') {
			
			//case 'userexists'

			case 'get':
				var entries = Entry.manager.all();
				var arr = new Array<Dynamic>();
				for( e in entries ) {
					arr.push({
						username : e.username,
						date : e.date,
						score : e.score
					});
				}
				print( Json.stringify( arr ) );
				return;

			case 'submit':
				var e = new Entry();
				e.username = params.get('username');
				e.score = Std.parseInt( params.get('score') );
				e.date = Date.now();
				e.ip = Web.getClientIP();
				e.uri = Web.getURI();
				e.insert();

			default:
				if( params.count() == 0 ) {
					print( 'error:no parameters' );
				} else {
					print( 'error:unknown parameters ($params)' );
				}
				return;
			}
			*/

			sys.db.Manager.cleanup();
			cnx.close();

		} catch(e:Dynamic) {
			print( 'error:'+e );
			return;
		}
		print("ok");
	}
}

#end
