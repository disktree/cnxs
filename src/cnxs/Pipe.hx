package cnxs;

import pixi.DisplayObjectContainer;
import pixi.Graphics;
import Tween;

enum PipeType {
	//empty;
	linear;
	angular;
}

class Pipe extends pixi.DisplayObjectContainer {

	public dynamic function onRotationStart( pipe : Pipe ) {}
	public dynamic function onRotationEnd( pipe : Pipe ) {}

	public var ix : Int;
	public var iy : Int;
	public var type(default,null) : PipeType;
	public var direction(default,null) : Int;
	public var active(get,set) : Bool;
	public var rotationDuration(default,null) : Float;
	public var ports(default,null) : Array<Int>;
	public var numConnections = 0;

	var _active = false;

	var size : Int;
	var thick : Int;
	var colorRegular : Int;
	var colorActive : Int;

	//var background : Graphics;
	var pipe : Graphics;
	//var pipe : pixi.Sprite;

	var tweenRotation : Tween;
	var isRotating = false;

	public function new( ix : Int, iy : Int,
						 type : PipeType, direction : Int,
						 size : Int, thick : Int,
						 colorRegular : Int, colorActive : Int ) {

		super();
		this.ix = ix;
		this.iy = iy;
		this.type = type;
		this.direction = direction;
		this.size = size;
		this.thick = thick;
		this.colorRegular = colorRegular;
		this.colorActive = colorActive;

		//background = new Graphics();
		//background.pivot = new pixi.Point( size/2, size/2 );
		//drawBackground( colorBackground );
		//addChild( background );

		pipe = new Graphics();
		pipe.pivot = new pixi.Point( size/2, size/2 );
		pipe.rotation = Math.PI/2 * direction;
		//pipe.alpha = 0.9;
		drawPipe( colorRegular );
		addChild( pipe );
		
		rotationDuration = 150;
		ports = new Array();

		updatePorts();
	}

	inline function get_active() return _active;
	function set_active(v:Bool) : Bool {
		if( v != _active ) drawPipe( v ? colorActive : colorRegular );
		return _active = v;
	}

	public function rotate( clockwise = true ) {

		if( isRotating ) {
			tweenRotation.stop();
			pipe.rotation = Math.PI/2 * direction;

			//isRotating = false;
			updatePorts();
			onRotationEnd( this );
			//return;
		}

		var rotationValue : Float = null;
		if( clockwise ) {
			if( ++direction == 4 ) direction = 0;
			rotationValue = pipe.rotation + Math.PI/2;
		} else {
			if( --direction == -1 ) direction = 3;
			rotationValue = pipe.rotation - Math.PI/2;
		}

		updatePorts();

		tweenRotation = new Tween( pipe )
			.to({ rotation : rotationValue }, rotationDuration )
			.easing( Easing.Quadratic.InOut )
			.onStart(function(){
				onRotationStart( this );
			})
			.onComplete(function(){
				onRotationEnd( this );
			});
		isRotating = true;
		tweenRotation.start();
	}

	public function remove( time : Int = 500, ?callback : Void->Void ) {
		var tween = new Tween( this )
			.to( {alpha:0}, time )
			.onComplete(function(){
				if( callback != null ) callback();
			});
		tween.start();
	}

	public function move( x : Float, y : Float, time = 1000 ) {
		new Tween( this ).to( {x:x,y:y}, time ).start();
	}

	function updatePorts() {
		switch type {
		case linear:
			ports = switch direction {
			case 0: [0,2];
			case 1: [1,3];
			case 2: [2,0];
			case 3: [3,1];
			default:null;
			}
		case angular:
			ports = switch direction {
			case 0: [0,1];
			case 1: [1,2];
			case 2: [2,3];
			case 3: [3,0];
			default:null;
			}
		}
	}

	/*
	function generateSpriteTexture( color : Int ) : pixi.Texture {
		var g = new Graphics();
		g.beginFill( color );
		switch type {
		case linear:
			g.drawRect( size/2-thick/2, 0, thick, size );
		case angular:
			g.drawRect( size/2-thick/2, 0, thick, size/2+thick/2 );
			g.drawRect( size/2+thick/2, size/2-thick/2, size/2-thick/2, thick );
		}
		g.endFill();
		return g.generateTexture( StatePlay.renderer );
	}
	*/

	function drawPipe( color : Int, alpha = 1.0 ) {
		pipe.clear();
		pipe.beginFill( color, alpha );
		switch type {
		case linear:
			pipe.drawRect( size/2-thick/2, 0, thick, size );
		case angular:
			pipe.drawRect( size/2-thick/2, 0, thick, size/2+thick/2 );
			pipe.drawRect( size/2+thick/2, size/2-thick/2, size/2-thick/2, thick );
		}
		pipe.endFill();
	}

	/*
	function onMouseOver(e) {
		//drawPipe( 0xfff000 );
		pipe.alpha = 1.0;
	}

	function onMouseOut(e) {
		pipe.alpha = 0.9;
	}
	*/

	function onMouseDown(e) {
	//	if( isRotating )
	//		return;
		//rotate( Board.rotateClockwise );
		//active = true;
	}
}
