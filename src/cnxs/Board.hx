package cnxs;

import js.Browser.window;
import js.html.CanvasElement;
import haxe.Timer;
import pixi.DisplayObjectContainer;
import pixi.Graphics;
import cnxs.Level;
import cnxs.Pipe;
import Tween;

using Lambda;

class Board extends DisplayObjectContainer {

	public static inline var SIDE_THICK = 10.0;

	//public static var colorBackground(default,null) : Int;
	public static var colorRegular(default,null) : Int;
	public static var colorHighlighted(default,null) : Int;
	public static var rotateClockwise = true;

	public dynamic function onSolve( pipes : Array<Pipe> ) {}

	var canvas : CanvasElement;
	var dimension : Int;
	var size : Int;
	var fieldSize : Int;
	var pipeThick : Int;
	var pipeTypeRatio : Float;
	var pipes : Array<Array<Pipe>>;
	var activePipes : Array<Pipe>;
	var isSolving = false;
	var paused = false;

	var background : Graphics;
	var pipeContainer : DisplayObjectContainer;
	var grid : BoardGrid;
	var boardSideTop : BoardSide;
	var boardSideRight : BoardSide;
	var boardSideBottom : BoardSide;
	var boardSideLeft : BoardSide;

	var pauseFilterGray : pixi.GrayFilter;
	var pauseFilterBlur : pixi.BlurFilter;

	public function new( canvas : CanvasElement, dimension : Int, size : Int ) {

		super();
		this.canvas = canvas;
		this.dimension = dimension;
		this.size = size - Std.int( SIDE_THICK * 2 );

		fieldSize = Std.int( this.size / dimension );
		pipeThick = Std.int( fieldSize/4 );

		background = new Graphics();
		addChild( background );

		boardSideTop = new BoardSide( dimension, this.size, fieldSize, pipeThick );
		boardSideTop.x = SIDE_THICK;
		//boardSideTop.y = -SIDE_THICK;
		addChild( boardSideTop );

		boardSideRight = new BoardSide( dimension, this.size, fieldSize, pipeThick );
		boardSideRight.rotation = Math.PI/2;
		//boardSideRight.x = size;// + SIDE_THICK;
		boardSideRight.y = SIDE_THICK;
		addChild( boardSideRight );

		boardSideBottom = new BoardSide( dimension, this.size, fieldSize, pipeThick );
		boardSideBottom.x = SIDE_THICK;
		//boardSideBottom.y = size - SIDE_THICK;
		addChild( boardSideBottom );

		boardSideLeft = new BoardSide( dimension, this.size, fieldSize, pipeThick );
		boardSideLeft.rotation = Math.PI/2;
		//boardSideLeft.x = SIDE_THICK;
		boardSideLeft.y = SIDE_THICK;
		addChild( boardSideLeft );
		
		grid = new BoardGrid();
		addChild( grid );

		pauseFilterGray = new pixi.GrayFilter(); 
		pauseFilterGray.gray = 0;
		pauseFilterBlur = new pixi.BlurFilter();
		pauseFilterBlur.blur = 0;

		if( App.MOBILE )
			canvas.addEventListener( 'touchstart', onTouch, false );
		else {
			canvas.addEventListener( 'contextmenu', onContextMenu, false );
			canvas.addEventListener( 'click', onClick, false );
		}
	}

	public function startLevel( level : LevelData ) {

		pipeTypeRatio = level.pipeTypeRatio;
		//colorBackground = level.colors.background;
		colorRegular = level.colors.pipeRegular;
		colorHighlighted = level.colors.pipeActive;

		background.clear();
		background.beginFill( level.colors.background );
		background.drawRect( 0, 0, size+SIDE_THICK*2, size+SIDE_THICK*2 );
		background.endFill();

		pipeContainer = new DisplayObjectContainer();
		pipeContainer.x = pipeContainer.y = SIDE_THICK;
		pipeContainer.alpha = 0;
		addChild( pipeContainer );

		pipes = new Array();
		
		for( ix in 0...dimension ) {
			var col = new Array<Pipe>();
			for( iy in 0...dimension ) {
				var pipe = createPipe( ix, iy );
				pipe.onRotationStart = onPipeRotationStart;
				pipe.onRotationEnd = onPipeRotationEnd;
				pipeContainer.addChild( pipe );
				col.push( pipe );
			}
			pipes.push( col );
		}

		var obj = {alpha:0.0};
		new Tween( obj ).to( {alpha:1.0}, 500 )
			.onUpdate(function(){
				pipeContainer.alpha = obj.alpha;
			}).start();
		
		grid.create( dimension, fieldSize, level.colors.grid );
		Timer.delay( function() grid.rotate( 1, 500 ), 120 );
		
		boardSideTop.y = -SIDE_THICK;
		boardSideRight.x = size + SIDE_THICK*3;
		boardSideBottom.y = size + SIDE_THICK*2;
		boardSideLeft.x = 0;

		var obj = {pos:0.0};
		new Tween( obj ).to( {pos:SIDE_THICK}, 500 )
			.onUpdate(function(){
				boardSideTop.y = -SIDE_THICK + obj.pos;
				boardSideRight.x = size + SIDE_THICK*3 - obj.pos;
				boardSideBottom.y = size + SIDE_THICK*2 - obj.pos;
				boardSideLeft.x = obj.pos;
			}).start();

		activePipes = new Array();

		for( col in pipes )
			for( pipe in col )
				onPipeRotationEnd( pipe );
		for( col in pipes )
			for( pipe in col )
				pipe.active = false;

		updateSidePipes();
	}
	
	public function endLevel( callback : Void->Void ) {

		var obj = { pos:0.0, alpha:1.0 };
		new Tween( obj ).to( { pos:SIDE_THICK, alpha:0.0 }, 500 )
			.onUpdate(function(){
				boardSideTop.y = - obj.pos;
				boardSideRight.x = size + SIDE_THICK*2 + obj.pos;
				boardSideBottom.y = size + SIDE_THICK + obj.pos;
				boardSideLeft.x = SIDE_THICK - obj.pos;
				boardSideTop.alpha = boardSideRight.alpha = boardSideBottom.alpha = boardSideLeft.alpha = obj.alpha;
			}).start();

		var obj2 = {alpha:1.0};
		new Tween( obj2 ).to( {alpha:0.0}, 500 )
			.onUpdate(function(){
				pipeContainer.alpha = obj2.alpha;
			}).onComplete( function(){
				trace("!!");
				for( col in pipes ) {
					for( pipe in col ) {
						pipeContainer.removeChild( pipe );
					}
				}
				pipes = new Array();
				boardSideTop.alpha = boardSideRight.alpha = boardSideBottom.alpha = boardSideLeft.alpha = 1;
				callback();
			}).start();
	}

	public function update( time : Float ) {
	}

	public function pause() {
		//trace( "pause" );
		filters = [pauseFilterGray,pauseFilterBlur];
		var obj = {v:0.0};
		new Tween( obj )
			.to({v:1}, 200 )
			.onUpdate(function(){
				pauseFilterGray.gray = obj.v;
				pauseFilterBlur.blur = obj.v * 30;
			}).start();
	}

	public function resume() {
		var obj = {v:1};
		new Tween( obj )
			.to({v:0}, 200 )
			.onUpdate(function(){
				pauseFilterGray.gray = obj.v;
				pauseFilterBlur.blur = obj.v * 30;
			})
			.onComplete(function(){
				filters = null;
			}).start();
	}

	function onPipeRotationStart( pipe : Pipe ) {
		pipe.active = true;
	}

	function onPipeRotationEnd( pipe : Pipe ) {

		for( col in pipes ) {
			for( p in col ) {
				p.active = false;
				p.numConnections = 0;
			}
		}

		activePipes = [pipe];
		resolveConnectedPipes( pipe );
		for( pipe in activePipes )
			pipe.active = true;

		var solved = false;

		if( activePipes.length > 3 ) {
			solved = true;
			for( p in activePipes ) {
				if( p.numConnections < 2 ) {
					solved = false;
					break;
				}
			}
			if( solved ) {
				isSolving = true;
				var activeColumnIndexes = new Array<Int>();
				for( pipe in activePipes ) {
					if( !activeColumnIndexes.has( pipe.ix ) )
						activeColumnIndexes.push( pipe.ix );
				}
				for( ix in activeColumnIndexes ) {
					var iy = dimension-1;
					while( iy >= 0 ) {
						var pipe = pipes[ix][iy];
						if( pipe.active ) {
							var numNewPipes = 0;
							var pipesToMove= new Array<Pipe>();
							var positionsToMove = new Array<{i:Int,y:Float}>();
							var ny = iy;
							while( ny >= 0 ) {
								var np = pipes[ix][ny];
								positionsToMove.push( { i:ny, y:np.y } );
								if( np.active ) {
									numNewPipes++;
								} else {
									pipesToMove.push( np );
								}
								ny--;
							}

							for( pipe in activePipes ) {
								pipe.remove( 200 );
							}

							var i = 0;
							for( pipe in pipesToMove ) {
								var pos = positionsToMove[i];
								pipe.iy = pos.i;
								pipes[ix][pos.i] = pipe;
								pipe.move( pipes[ix][pos.i].x, pos.y, 200 );
								i++;
							}

							for( i in 0...numNewPipes ) {
								var newPipe = createPipe( ix, i );
								newPipe.onRotationEnd = onPipeRotationEnd;
								pipeContainer.addChild( newPipe );
								pipes[ix][i] = newPipe;
								newPipe.x = ix * fieldSize + fieldSize/2;
								newPipe.y = -fieldSize;
								newPipe.move( ix * fieldSize + fieldSize/2, i * fieldSize + fieldSize/2, 200  );
							}
							
							break;
						}
						iy--;
					}
				}
			}
		}

		if( solved ) {

			grid.rotate();
			//Sound.play( 'Game FX 09' );

			onSolve( activePipes );

			Timer.delay( function(){
				isSolving = false;
				for( col in pipes )
					for( pipe in col )
						onPipeRotationEnd( pipe );
				for( col in pipes )
					for( pipe in col )
						pipe.active = false;
				updateSidePipes();

			}, 200 );
		}
		updateSidePipes();
	}

	function resolveConnectedPipes( pipe : Pipe ) {
		for( port in pipe.ports ) {
			var neighbour : Pipe = switch port {
				case 0: pipes[pipe.ix][if(pipe.iy == 0) dimension-1 else pipe.iy-1];
				case 1: pipes[if(pipe.ix == dimension-1) 0 else pipe.ix+1][pipe.iy];
				case 2: pipes[pipe.ix][if( pipe.iy == dimension-1) 0 else pipe.iy+1];
				case 3: pipes[if(pipe.ix == 0) dimension-1 else pipe.ix-1][pipe.iy];
				default:
			}
			var connected = false;
			for( neighbourPort in neighbour.ports ) {
				switch port {
				case 0: if( neighbourPort == 2 ) connected = true;
				case 1: if( neighbourPort == 3 ) connected = true;
				case 2: if( neighbourPort == 0 ) connected = true;
				case 3: if( neighbourPort == 1 ) connected = true;
				}
			}
			if( connected ) {
				neighbour.active = true;
				neighbour.numConnections++;
				if( !activePipes.has( neighbour ) ) {
					activePipes.push( neighbour );
					resolveConnectedPipes( neighbour );
				}
			}
		}
	}

	function createPipe( ix : Int, iy : Int ) : Pipe {
		var type = randomBool( pipeTypeRatio ) ? angular : linear;
		var direction = Std.int( Math.random() * 4 );
		var pipe = new Pipe( ix, iy, type, direction, fieldSize, pipeThick, colorRegular, colorHighlighted );
		pipe.x = ix * fieldSize + fieldSize/2;
		pipe.y = iy * fieldSize + fieldSize/2;
		return pipe;
	}

	function updateSidePipes() {
		
		// Top
		var sidePipeIndexes = new Array<Bool>();
		for( i in 0...dimension ) {
			var p = pipes[i][0];
			sidePipeIndexes.push( p.active && p.ports.has(0) );
		}
		for( i in 0...dimension ) {
			var p = pipes[i][dimension-1];
			if( p.active && p.ports.has(2) ) sidePipeIndexes[i] = true;
		}
		boardSideTop.update( sidePipeIndexes );

		// Right
		sidePipeIndexes = new Array<Bool>();
		for( i in 0...dimension ) {
			var p = pipes[dimension-1][i];
			sidePipeIndexes.push( p.active && p.ports.has(1) );
		}
		for( i in 0...dimension ) {
			var p = pipes[0][i];
			if( p.active && p.ports.has(3) ) sidePipeIndexes[i] = true;
		}
		boardSideRight.update( sidePipeIndexes );

		// Bottom
		var sidePipeIndexes = new Array<Bool>();
		for( i in 0...dimension ) {
			var p = pipes[i][dimension-1];
			sidePipeIndexes.push( p.active && p.ports.has(2) );
		}
		for( i in 0...dimension ) {
			var p = pipes[i][0];
			if( p.active && p.ports.has(0) ) sidePipeIndexes[i] = true;
		}
		boardSideBottom.update( sidePipeIndexes );

		// Left
		sidePipeIndexes = new Array<Bool>();
		for( i in 0...dimension ) {
			var p = pipes[0][i];
			sidePipeIndexes.push( p.active && p.ports.has(3) );
		}
		for( i in 0...dimension ) {
			var p = pipes[dimension-1][i];
			if( p.active && p.ports.has(1) ) sidePipeIndexes[i] = true;
		}
		boardSideLeft.update( sidePipeIndexes );
	}

	function onClick(e) {
		e.preventDefault();
		e.stopPropagation();
		if( paused || isSolving )
			return;
		_onClick( e.offsetX - fieldSize/2 + SIDE_THICK, e.offsetY - fieldSize/2 + SIDE_THICK );
	}

	function onTouch(e) {
		e.preventDefault();
		e.stopPropagation();
		if( paused || isSolving )
			return;
		_onClick( e.touches[0].pageX, e.touches[0].pageY );
	}

	function _onClick( x : Float, y : Float ) {
		var pipe = getPipeUnderPosition( x, y );
		if( pipe != null ) {
			pipe.rotate( rotateClockwise );
		}
	}

	function onContextMenu(e) {
		e.preventDefault();
		rotateClockwise = false;
		onClick(e);
		rotateClockwise = true;
		return false;
	}

	function getPipeUnderPosition( x : Float, y : Float ) : Pipe {
		//if( x > size ) return null;
		var ix = Std.int( x / fieldSize );
		var iy = Std.int( y / fieldSize );
		if( ix >= dimension || iy >= dimension )
			return null;
		return pipes[ix][iy];
	}

	static inline function randomBool( factor = 0.5 ) : Bool {
		return (Math.random()*1.0) < factor;
	}
}

private class BoardSide extends DisplayObjectContainer {

	var pipes : Array<BoardSidePipe>;

	public function new( dimension : Int, size : Int, fieldSize : Int, pipeThick : Int ) {
		super();
		pipes = new Array();
		for( i in 0...dimension ) {
			var pipe = new BoardSidePipe( pipeThick, Std.int( Board.SIDE_THICK ) );
			pipe.x = i * fieldSize + fieldSize/2 - pipeThick/2;
			addChild( pipe );
			pipes.push( pipe );
		}
	}

	public function update( indexes : Array<Bool> ) {
		for( i in 0...indexes.length ) {
			pipes[i].draw( indexes[i] ? Board.colorHighlighted : Board.colorRegular );
		}
	}
}

private class BoardSidePipe extends Graphics {

	var pipeWidth : Int;
	var pipeHeight : Int;

	public function new( width : Int, height : Int ) {
		super();
		this.pipeWidth = width;
		this.pipeHeight = height;
	}

	public function draw( color : Int ) {
		clear();
		beginFill( color );
		drawRect( 0, 0, pipeWidth, pipeHeight );
		endFill();
	}
}

private class BoardGrid extends DisplayObjectContainer {

	var crosses : Array<Array<BoardGridCross>>;

	public function new() {
		super();
	}

	public function create( dimension : Int, fieldSize : Int, color : Int ) {
		if( crosses != null ) {
			for( col in crosses ) {
				for( cross in col ) {
					removeChild( cross );
					cross = null;
				}
			}
		}
		crosses = new Array();
		for( i in 0...dimension+1 ) {
			var col = new Array<BoardGridCross>();
			for( j in 0...dimension+1 ) {
				var cross = new BoardGridCross( color );
				cross.x = i * fieldSize + Board.SIDE_THICK;
				cross.y = j * fieldSize + Board.SIDE_THICK;
				addChild( cross );
				col.push( cross );
			}
			crosses.push( col );
		}
	}

	public function rotate( n = 1.0, time = 200.0 ) {
		var v = {rot:0.0};
		new Tween(v)
			.to( {rot:n}, time )
			.easing( Easing.Quadratic.InOut )
			.onUpdate(function(){
				for( col in crosses ) {
					for( cross in col ) {
						cross.rotation = v.rot * Math.PI*2;
					}
				}
			}).start();
	}
}

private class BoardGridCross extends pixi.Sprite {

	public static inline var SIZE = 10;

	public function new( color : Int ) {
		var sh = SIZE/2;
		var g = new Graphics();
		g.lineStyle( 1, color, 1 );
		g.moveTo( sh, 0 );
		g.lineTo( sh, SIZE );
		g.moveTo( 0, sh );
		g.lineTo( SIZE, sh );
		super( g.generateTexture( StatePlay.renderer ) );
		anchor = new pixi.Point( 0.5, 0.5 );
	}
}
