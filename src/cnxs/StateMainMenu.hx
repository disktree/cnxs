package cnxs;

import js.Browser.document;
import js.html.DivElement;
import js.html.SpanElement;

@:keep
class StateMainMenu extends State {

	var e_content : DivElement;
	var e_buttons : DivElement;

	public override function init() {

		super.init();

		if( App.MOBILE ) {
			untyped document.body.webkitRequestFullscreen();
		}

		e.style.width = App.WIDTH+'px';
		e.style.height = App.HEIGHT+'px';
		//e.style.margin = '10px';
		
		e_content = document.createDivElement();
		e_content.id = 'mainmenu-content';
		e.appendChild( e_content );

		//var e_game = addButton( 'PLAY', StateGame );

		var e_buttons = document.createDivElement();
		e_content.appendChild( e_buttons );

		var e_start = createButton( 'START' );
		e_start.onclick = function(_){
			State.set( StatePlay );
		}
		e_buttons.appendChild( e_start );

		/*
		var e_resume = createButton( 'RESUME' );
		e_buttons.appendChild( e_resume );

		var e_free = createButton( 'FREE' );
		e_buttons.appendChild( e_free );
		*/

		var scoreBoard = new HighscoreBoard();
		e_content.appendChild( scoreBoard.e );

		/*
		var e_googleplus = document.createDivElement();
		e_googleplus.id = 'g-plusone';
		e_googleplus.innerHTML = '<g:plusone></g:plusone>';
		//e_googleplus.innerHTML = '<div class="g-plusone" data-size="medium" data-annotation="none"></div>';
		e.appendChild( e_googleplus );

		untyped gapi.plusone.go("google_plus_button");
		*/


		var e_wtf = document.createImageElement();
		e_wtf.id = 'wtf';
		e_wtf.src = 'images/wtf.png';
		e_content.appendChild( e_wtf );

		var e_betawarning = document.createDivElement();
		e_betawarning.style.position = 'fixed';
		e_betawarning.style.left = '2px';
		e_betawarning.style.bottom = '1px';
		e_betawarning.style.fontSize = '9px';
		e_betawarning.innerText = 'V'+Cnxs.VERSION+' - WORK IN PROGRESS';
		e.appendChild( e_betawarning );
		
		//Sound.play('Birdscape C.ogg', 0.2, true );
		//Music.play( 'Birdscape C.ogg', 0.2 );
	}

	public override function close() {
	}

	function createButton( text : String ) : SpanElement {
		var e = document.createSpanElement();
		e.classList.add( 'button' );
		e.innerText = text;
		return e;
	}

	/*
	function addButton( text : String, state : Class<State> ) {
		var e = document.createSpanElement();
		e.classList.add( 'button' );
		e.innerText = text;
		//e.addEventListener( 'click', function(_){}, false );
		e.onclick = function(_) {
			State.set( state );
		}
		e_content.appendChild( e );
		return e;

	}
	*/

}

/*
private class ScoreBoard {

	static inline var MAX_ENTRIES_TO_SHOW = 10;

	public var e(default,null) : DivElement;
	
	public function new() {

		e = document.createDivElement();
		e.id = 'scoreboard';

		load();
	}

	function load() {
		Highscores.get( function(data){
			for( entry in data ) {
				
				var user = entry.user;
				var dateTime = xTimeAgo( Date.fromString( entry.date ) );
				var score = entry.score;

				var e = document.createDivElement();
				var html = '<div class="entry"><span class="user">$user</span>/<span class="score">$score</span>/<span class="date">$dateTime</span></div>';
				e.innerHTML = html;
				this.e.appendChild(e);
			}
		});
	}

	static function xTimeAgo( date : Date ) : String {
		var diff = ( Date.now().getTime() - date.getTime() ) / 1000;
		var day_diff = Math.floor( diff / 86400 );
		if( Math.isNaN( day_diff ) || day_diff < 0 || day_diff >= 31 )
			return null;
		if( day_diff == 0 ) {
			if( diff < 60 ) return "just now";
			else if( diff < 120 ) return "1 minute ago";
			else if( diff < 3600 ) return Math.floor( diff/60 ) + " minutes ago";
			else if( diff < 7200 ) return "1 hour ago";
			else if( diff < 86400 ) return Math.floor( diff/3600 )+" hours ago";
		} else {
			if( day_diff == 1 ) return "Yesterday";
			else if( day_diff < 7 ) return day_diff+" days ago";
			else if( day_diff < 31 ) return Math.ceil( day_diff/7)+" weeks ago";
		}
		return null;
	}

}
*/

//private class Advertising {
